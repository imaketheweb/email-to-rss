module.exports = {
  parser: 'babel-eslint',
  plugins: ['cypress', 'import'],
  env: {
    'cypress/globals': true,
  },
  extends: [
    'airbnb',
    'prettier',
    'jsdoc-strict',
    'jest-enzyme',
    'cypress',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  rules: {
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'jsdoc/require-example': 'warning',
  },
}
