module.exports = {
  snapshotSerializers: ['enzyme-to-json/serializer'],
  coverageDirectory: '__coverage__',
  collectCoverageFrom: ['<rootDir>pages', '<rootDir>components'],
  testPathIgnorePatterns: [
    '<rootDir>/build/',
    '<rootDir>/node_modules/',
    '<rootDir>/cypress/',
  ],

  // identify test setup file for Enzyme adapter and other needs
  setupFiles: ['<rootDir>jest.setup.js'],
}
