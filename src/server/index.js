/**
 * @file App index
 */

const Koa = require('koa')
const logger = require('koa-logger')
const Router = require('koa-router')
const bodyParser = require('koa-bodyparser')
const Next = require('next')

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'

/* Next process */
const nextApp = Next({ dir: './src', dev })

const handle = nextApp.getRequestHandler()

/* wrap Express in Next */
nextApp.prepare().then(() => {
  const app = new Koa()
  const router = new Router()

  /* logging */
  app.use(logger())
  /* body parsing */
  app.use(bodyParser())

  router.get('*', async ctx => {
    await handle(ctx.req, ctx.res)
    ctx.respond = false
  })

  app.use(async (ctx, next) => {
    ctx.res.statusCode = 200
    await next()
  })

  app.use(router.routes())
  app.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
})
