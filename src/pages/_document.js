import React from 'react'

import Document, { Head, Main, NextScript } from 'next/document'

/**
 * Renders Document component.
 * Rendered only on the server side and not on the client side.
 */
export default class CustomDocument extends Document {
  /**
   * Gets the initial props.
   *
   * @param {Object} ctx - Context.
   *
   * @returns {Object} Initial props.
   */
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  /**
   * Renders component.
   *
   * @returns {Function} HTML component.
   */
  render() {
    return (
      <html lang="en">
        <Head>
          <link rel="stylesheet" href="../../static/bulma.min.css" />
        </Head>
        <body className="custom_class">
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
