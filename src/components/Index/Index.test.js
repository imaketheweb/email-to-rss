/**
 * @file Sign Up component tests
 */

import React from 'react'
import { render } from 'enzyme'

import Index from './Index'

describe('index', () => {
  it('should render the index component', () => {
    const wrapper = render(<Index />)

    expect(wrapper).toMatchSnapshot()
  })
})
