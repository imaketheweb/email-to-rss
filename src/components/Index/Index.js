/**
 * @file Sign Up component
 */

import React from 'react'

/**
 * Renders Index component.
 *
 * @example
 * <Index />
 *
 * @returns {Function} Index component.
 */
const Index = () => <h3 className="title">Index</h3>

export default Index
