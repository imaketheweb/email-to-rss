/**
 * @file Index end-to-end tests
 */

describe('/', () => {
  it('loads index page', () => {
    cy.visit('http://localhost:3000/')
      .get('h3.title')
      .should('contain', 'Index')
  })
})
